import React from 'react';
import PropTypes from 'prop-types';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

const ThemeStyles = {
  color: {
    // BASE
    black: '#000',
    darkgray: '#282828',
    lightgray: '#999',
    white: '#fff',
    // SKILL TREE
    blue: '#23458B',
    lightblue: '#4472D0',
    green: '#105600',
    lightgreen: '#26BD00',
    red: '#7E2F15',
    lightred: '#D64D24',
    purple: '#6C206C',
    // OTHER
    yellow: '#EFCA58',
    darkBlue: '#18314D',
  },
};

export const Theme = ({ children }) => (
  <ThemeProvider theme={ThemeStyles}>{children}</ThemeProvider>
);

export const GlobalStyles = createGlobalStyle`
  /* Box sizing rules */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }

  /* Remove default padding */
  ul[class],
  ol[class] {
    padding: 0;
  }

  /* Remove default margin */
  body,
  h1,
  h2,
  h3,
  h4,
  p,
  ul[class],
  ol[class],
  li,
  figure,
  figcaption,
  blockquote,
  dl,
  dd {
    margin: 0;
  }

  /* Set core body defaults */
  body {
    background: #181818;
    color: white;
    font-family: 'Titillium Web', sans-serif;
    line-height: 1.5;
    height: 100vh;
    overflow-x: hidden;
    scroll-behavior: smooth;
    text-rendering: optimizeSpeed;
    width: 100vw;
  }

  #root {
    overflow-x: hidden;
    min-height: 100vh;
    padding: 0 0 220px;
  }

  /* Remove list styles on ul, ol elements with a class attribute */
  ul[class],
  ol[class] {
    list-style: none;
  }

  /* A elements that don't have a class get default styles */
  a:not([class]) {
    text-decoration-skip-ink: auto;
  }

  /* Make images easier to work with */
  img {
    max-width: 100%;
    display: block;
  }

  /* Natural flow and rhythm in articles by default */
  article > * + * {
    margin-top: 1em;
  }

  /* Inherit fonts for inputs and buttons */
  input,
  button,
  textarea,
  select {
    font: inherit;
  }

  /* Remove button defaults */
  button {
    border: none;
    background: none;
  }

  /* Hide Scroll Bars */
  * {
    -ms-overflow-style: none; /* IE and Edge */
    scrollbar-width: none;
  }
  *::-webkit-scrollbar {
    display: none;
  }

  /* Remove all animations and transitions for people that prefer not to see them */
  @media (prefers-reduced-motion: reduce) {
    * {
      animation-duration: 0.01ms !important;
      animation-iteration-count: 1 !important;
      transition-duration: 0.01ms !important;
      scroll-behavior: auto !important;
    }
  }
`;

Theme.propTypes = {
  children: PropTypes.element.isRequired,
};
