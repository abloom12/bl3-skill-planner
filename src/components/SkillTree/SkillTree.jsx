import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { getSpentTreePoints } from 'redux/skills/selectors';

import TreeSkill from './TreeSkill';

import * as S from './SkillTree.styles';

const Tree = props => {
  const spentPoints = useSelector(state =>
    getSpentTreePoints(state, {
      hunter: props.hunter,
      treeColor: props.treeColor,
    }),
  );

  return (
    <S.Tree color={props.treeColor}>
      {props.skillIds.map(skillId => (
        <TreeSkill
          key={skillId}
          skillId={skillId}
          hunter={props.hunter}
          spentPointsInTree={spentPoints}
        />
      ))}
    </S.Tree>
  );
};

Tree.propTypes = {
  hunter: PropTypes.string.isRequired,
  skillIds: PropTypes.array.isRequired,
  treeColor: PropTypes.string.isRequired,
};

export default Tree;
