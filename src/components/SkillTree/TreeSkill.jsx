import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { setActiveSkill } from 'redux/active/actions';
import { getSkillData } from 'redux/skills/selectors';

import Skill from 'components/Skill';
import * as S from './TreeSkill.styles';

const isEnabled = (tier, spentPointsInTree) => {
  if (tier > 1) {
    const pointsToEnable = (tier - 1) * 5;
    return pointsToEnable <= spentPointsInTree ? true : false;
  }

  return true;
};

const TreeSkill = props => {
  const dispatch = useDispatch();

  const skillData = useSelector(state =>
    getSkillData(state, { id: props.skillId, hunter: props.hunter }),
  );

  const enabled = isEnabled(skillData.tier, props.spentPointsInTree);

  const handleClick = () => {
    dispatch(setActiveSkill(skillData.id));
  };

  return (
    <S.TreeSkill
      gridArea={`${skillData.tier}${skillData.tierColumn}`}
      enabled={enabled}
      onClick={handleClick}>
      <Skill
        equipped={skillData.equipped}
        image={skillData.image}
        imageBackground={skillData.imageBackground}
        maxRank={skillData.maxRank}
        purchased={skillData.isPurchased}
        rank={skillData.rank}
        treeColor={skillData.treeColor}
        type={skillData.type}
      />
    </S.TreeSkill>
  );
};

TreeSkill.propTypes = {
  hunter: PropTypes.string.isRequired,
  skillId: PropTypes.string.isRequired,
  spentPointsInTree: PropTypes.number.isRequired,
};

export default TreeSkill;
