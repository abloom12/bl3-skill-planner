import styled from 'styled-components';

export const TreeSkill = styled.div`
  grid-area: ${({ gridArea }) => `c${gridArea}`};
  touch-action: manipulation;
  filter: drop-shadow(-3px 0px 3px rgba(0, 0, 0, 1));

  ${({ enabled }) => (!enabled ? 'opacity: 0.5' : '')}
`;
