import styled from 'styled-components';

export const Tree = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 20%);
  grid-template-rows: auto;
  grid-template-areas:
    'c00 c01 c02 c03 c04'
    'c10 c11 c12 c13 c14'
    'c20 c21 c22 c23 c24'
    'c30 c31 c32 c33 c34'
    'c40 c41 c42 c43 c44'
    'c50 c51 c52 c53 c54'
    'c60 c61 c62 c63 c64';

  position: relative;

  &::after {
    background: ${({ color, theme }) => theme.color[color]};
    box-shadow: inset 0px 0px 10px 1px ${({ theme }) => theme.color.black};
    border-radius: 10px;
    content: '';
    height: calc(100% - 30px);
    opacity: 1;
    position: absolute;
    top: 48px;
    left: 50%;
    transform: translateX(-50%);
    width: 70%;
  }

  &::after {
    z-index: -1;
  }
`;
