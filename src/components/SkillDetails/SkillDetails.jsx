import React from 'react';
import PropTypes from 'prop-types';

import * as S from './SkillDetails.styles';

const Details = props => {
  return (
    <>
      <S.Description>{props.skillText}</S.Description>
    </>
  );
};

Details.propTypes = {
  skillText: PropTypes.string.isRequired,
};

export default Details;
