import styled from 'styled-components';

export const Description = styled.p`
  font-size: 18px;
  margin: 10px 0 12px;
`;
