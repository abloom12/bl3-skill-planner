import styled from 'styled-components';

export const EquippedSkills = styled.div`
  box-shadow: 0px 10px 6px -8px #888, 0px -10px 6px -8px #888;
  overflow-y: auto;
`;

export const Skill = styled.div`
  display: flex;
  position: relative;
  margin: 0;
`;

export const Image = styled.div`
  margin: 0 6px 0 0;
  max-width: 75px;
`;

export const Name = styled.div`
  align-self: center;
  flex-grow: 1;
  position: relative;
  top: -5px;
`;
