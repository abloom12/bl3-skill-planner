import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { getAllEquippedSkills } from 'redux/skills/selectors';

import Skill from 'components/Skill/Skill';

import * as S from './EquippedSkills.styles';

const EquippedSkills = props => {
  const { abilities, passives } = useSelector(state =>
    getAllEquippedSkills(state, { hunter: props.hunter }),
  );

  return (
    <S.EquippedSkills>
      <h1>Equipped Skills</h1>
      <div>
        <h2>Abilities</h2>
        {abilities.map(skillData => (
          <S.Skill key={skillData.id}>
            <S.Image>
              <Skill
                equipped={skillData.equipped}
                image={skillData.image}
                imageBackground={skillData.imageBackground}
                maxRank={skillData.maxRank}
                purchased={skillData.isPurchased}
                rank={skillData.rank}
                treeColor={skillData.treeColor}
                type={skillData.type}
              />
            </S.Image>
          </S.Skill>
        ))}
      </div>
      <div>
        <h2>Passives</h2>
        {passives.map(skillData => (
          <S.Skill key={skillData.id}>
            <S.Image>
              <Skill
                equipped={skillData.equipped}
                image={skillData.image}
                imageBackground={skillData.imageBackground}
                maxRank={skillData.maxRank}
                purchased={skillData.isPurchased}
                rank={skillData.rank}
                treeColor={skillData.treeColor}
                type={skillData.type}
              />
            </S.Image>
          </S.Skill>
        ))}
      </div>
    </S.EquippedSkills>
  );
};
EquippedSkills.propTypes = {
  hunter: PropTypes.string.isRequired,
};

export default EquippedSkills;
