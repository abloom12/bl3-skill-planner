import styled from 'styled-components';
import { motion } from 'framer-motion';

export const Carousel = styled(motion.div)`
  margin: 0 auto;
  max-width: 376px;
  position: relative;
  padding-bottom: 600px;
`;

export const Item = styled(motion.div)`
  position: absolute;
  width: 100%;
`;

export const Nav = styled.div`
  display: flex;
  justify-content: space-between;
  left: 50%;
  margin: 0 auto;
  position: absolute;
  width: 98%;
  transform: translateX(-50%);
  z-index: 3;

  button {
    border: none;
    background: none;
    color: white;
    font-size: 26px;
    margin: 0;
    text-transform: uppercase;
  }
`;
