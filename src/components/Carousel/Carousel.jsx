import React, {
  createContext,
  useContext,
  useCallback,
  useState,
  useMemo,
} from 'react';
import PropTypes from 'prop-types';

import * as S from './Carousel.styles';

const CarouselContext = createContext();

const variants = {
  left: {
    x: '-36%',
    opacity: 0.2,
    scale: 0.9,
    zIndex: 1,
  },
  center: {
    x: 0,
    opacity: 1,
    scale: 1,
    zIndex: 2,
  },
  right: {
    x: '36%',
    opacity: 0.2,
    scale: 0.9,
    zIndex: 1,
  },
  back: {
    x: 0,
    opacity: 0,
    scale: 0.9,
    zIndex: 1,
  },
};

const layouts = {
  0: { 0: 'center', 1: 'left', 2: 'back', 3: 'right' },
  1: { 0: 'right', 1: 'center', 2: 'left', 3: 'back' },
  2: { 0: 'back', 1: 'right', 2: 'center', 3: 'left' },
  3: { 0: 'left', 1: 'back', 2: 'right', 3: 'center' },
};

const Carousel = ({ children, items }) => {
  const [activeItem, setActiveItem] = useState(0);

  const onClick = useCallback(
    direction => {
      setActiveItem(prevState =>
        prevState === items.length - 1 && direction > 0
          ? 0
          : prevState === 0 && direction < 0
          ? items.length - 1
          : prevState + direction,
      );
    },
    [items.length],
  );

  const value = useMemo(() => ({ onClick, activeItem }), [activeItem, onClick]);

  return (
    <CarouselContext.Provider value={value}>
      <S.Carousel>{children}</S.Carousel>
    </CarouselContext.Provider>
  );
};
Carousel.prototype = {
  children: PropTypes.element,
  items: PropTypes.array,
};

const Nav = () => {
  const { onClick } = useContext(CarouselContext);

  return (
    <S.Nav>
      <button onClick={() => onClick(1)}>{'<<'}</button>
      <button onClick={() => onClick(-1)}>{'>>'}</button>
    </S.Nav>
  );
};

const Item = ({ children, i }) => {
  const { activeItem } = useContext(CarouselContext);

  return (
    <S.Item
      initial="center"
      variants={variants}
      animate={layouts[activeItem][i]}>
      {children}
    </S.Item>
  );
};
Item.prototype = {
  children: PropTypes.element,
  i: PropTypes.number,
  key: PropTypes.string,
};

Carousel.Item = Item;
Carousel.Nav = Nav;

export default Carousel;
