import React from 'react';
import PropTypes from 'prop-types';

import * as S from './SkillInfo.styles';

const SkillInfo = props => {
  return (
    <S.Info>
      <S.Name>{props.name}</S.Name>
      <S.Type>{props.type.replaceAll('_', ' ')}</S.Type>
    </S.Info>
  );
};

SkillInfo.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default SkillInfo;
