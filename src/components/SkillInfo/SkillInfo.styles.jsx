import styled from 'styled-components';

export const Info = styled.div`
  align-self: center;
  flex-grow: 1;
  position: relative;
`;
export const Name = styled.div`
  font-size: 22px;
  font-weight: 600;
  line-height: 1;
  margin: 0 0 4px;
  text-transform: uppercase;
`;
export const Type = styled.div`
  color: ${({ theme }) => theme.color.yellow};
  font-size: 16px;
  line-height: 1;
  text-transform: uppercase;
`;
