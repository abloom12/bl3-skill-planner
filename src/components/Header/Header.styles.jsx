import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Header = styled.div`
  margin: 0 0 20px;
`;

export const NavLink = styled(Link)`
  color: white;
  font-size: 22px;
  margin: 0 10px 0 0;
  text-decoration: none;

  &::nth-child(0) {
    margin: 0;
  }
`;
