import React from 'react';

import * as S from './Header.styles';

const Header = () => {
  return (
    <S.Header>
      <S.NavLink to="/">Home</S.NavLink>
      <S.NavLink to="/amara">Amara</S.NavLink>
      <S.NavLink to="/fl4k">Fl4k</S.NavLink>
      <S.NavLink to="/moze">Moze</S.NavLink>
      <S.NavLink to="/zane">Zane</S.NavLink>
    </S.Header>
  );
};

export default Header;
