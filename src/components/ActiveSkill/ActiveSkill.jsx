import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import PropTypes from 'prop-types';

import { getSkillData } from 'redux/skills/selectors';
import { getActiveSkillId } from 'redux/active/selectors';

import Skill from '../Skill';
import SkillInfo from '../SkillInfo';
import SkillDetails from '../SkillDetails';
import SkillEffects from '../SkillEffects';
import Modal from '../Modal';

import SideBar from './SideBar';
import * as S from './ActiveSkill.styles';

const variants = {
  open: { height: 106 },
  closed: { height: 300 },
};

const ResponsiveWrapper = ({ isMobile, children }) => {
  return <>{isMobile ? <Modal>{children}</Modal> : children}</>;
};

const ActiveSkill = props => {
  const [isOpen, setIsOpen] = useState(false);
  const isMobile = useMediaQuery({ maxWidth: 930 });

  const activeSkillId = useSelector(state => getActiveSkillId(state));
  const skillData = useSelector(state =>
    getSkillData(state, { id: activeSkillId, hunter: props.hunter }),
  );

  return (
    <>
      {activeSkillId ? (
        <ResponsiveWrapper isMobile={isMobile}>
          <S.ActiveSkill>
            {props.isMobile ? <S.TopBar>hey</S.TopBar> : null}
            <>
              <S.Main>
                <S.Header>
                  <Skill
                    image={skillData.image}
                    imageBackground={skillData.imageBackground}
                    maxRank={skillData.maxRank}
                    purchased={skillData.isPurchased}
                    rank={skillData.rank}
                    treeColor={skillData.treeColor}
                    type={skillData.type}
                  />

                  <SkillInfo name={skillData.name} type={skillData.type} />
                </S.Header>

                <S.Body>
                  <SkillDetails skillText={skillData.text} />

                  {skillData.effects ? (
                    <SkillEffects
                      effects={skillData.effects}
                      skillType={skillData.type}
                      skillRank={skillData.rank}
                      skillMaxRank={skillData.maxRank}
                    />
                  ) : null}
                </S.Body>
              </S.Main>

              <S.SideBar>
                <SideBar
                  skillId={skillData.id}
                  skillType={skillData.type}
                  skillMaxRank={skillData.maxRank}
                  skillRank={skillData.rank}
                  hunter={skillData.hunter}
                />
              </S.SideBar>
            </>
          </S.ActiveSkill>
        </ResponsiveWrapper>
      ) : null}
    </>
  );
};

ActiveSkill.propTypes = {
  skillId: PropTypes.string.isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default ActiveSkill;
