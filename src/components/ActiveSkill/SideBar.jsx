import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import {
  addSkillPoint,
  removeSkillPoint,
  equipSkill,
} from 'redux/skills/actions';
import * as types from 'constants/index';

import { IconContext } from 'react-icons';
import { MdAdd, MdRemove, MdClear } from 'react-icons/md';

import * as S from './SideBar.styles';

const SideBar = props => {
  const dispatch = useDispatch();

  const handleClick = addRemove => {
    const payload = {
      id: props.skillId,
      hunter: props.hunter,
    };

    if (props.skillType === types.PASSIVE_ABILITY) {
      if (addRemove === 1 && props.skillRank < props.skillMaxRank) {
        dispatch(addSkillPoint(payload));
        return;
      }
      if (addRemove === -1 && props.skillRank > 0) {
        dispatch(removeSkillPoint(payload));
        return;
      }
    } else {
      payload.type = props.skillType;
      dispatch(equipSkill(payload));
    }
  };

  return (
    <S.Buttons>
      <IconContext.Provider
        value={{
          style: { verticalAlign: 'middle', fontSize: '40px' },
        }}>
        {props.skillType === types.PASSIVE_ABILITY ? (
          <>
            <MdAdd onClick={() => handleClick(1)} />
            <MdRemove onClick={() => handleClick(-1)} />
          </>
        ) : (
          <>
            <MdAdd onClick={() => handleClick()} />
          </>
        )}
      </IconContext.Provider>
    </S.Buttons>
  );
};

SideBar.propTypes = {
  skillId: PropTypes.string.isRequired,
  skillType: PropTypes.string.isRequired,
  skillMaxRank: PropTypes.number.isRequired,
  skillRank: PropTypes.number.isRequired,
  hunter: PropTypes.string.isRequired,
};

export default SideBar;
