import React from 'react';

import { IconContext } from 'react-icons';
import { MdExpandLess, MdExpandMore } from 'react-icons/md';

const TopBar = props => {
  return (
    <>
      <IconContext.Provider
        value={{
          style: { verticalAlign: 'middle', fontSize: '20px' },
        }}>
        <MdExpandLess />
      </IconContext.Provider>
    </>
  );
};

export default TopBar;
