import styled from 'styled-components';
import { motion } from 'framer-motion';

export const ActiveSkill = styled.div`
  background: ${({ theme }) => theme.color.darkgray};
  color: ${({ theme }) => theme.color.white};
  display: flex;
  flex-flow: row wrap;
  height: 220px;
`;

export const TopBar = styled.div`
  background: rgba(0, 0, 0, 0.2);
  text-align: center;
  width: 100%;
`;

export const Main = styled.div`
  flex-basis: 80%;
  flex-grow: 1;
  overflow-x: hidden;
  overflow-y: auto;
  padding: 6px 12px 0;
`;

export const Header = styled.div`
  display: flex;

  > div:last-child {
    margin: 0 0 0 8px;
  }
`;

export const Body = styled(motion.div)``;

export const SideBar = styled.div`
  background: rgba(0, 0, 0, 0.1);
  flex-basis: 20%;
  max-width: 50px;
  position: relative;
`;
