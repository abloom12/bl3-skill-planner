import styled from 'styled-components';

export const Buttons = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-self: center;
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translate3d(-50%, 0, 0);

  svg:nth-child(2) {
    margin-top: 16px;
  }
`;
