import React from 'react';

import {
  AnimatedRoutes,
  RouteTransition,
} from './AnimatedRoutes/AnimatedRoutes';

import Header from './Header/Header';
import Home from 'pages/Home/Home';
import Hunter from 'pages/Hunter/Hunter';

const hunters = ['amara', 'fl4k', 'moze', 'zane'];

const App = () => {
  return (
    <>
      <Header />
      <AnimatedRoutes exitBeforeEnter={true} initial={true}>
        <RouteTransition exact={true} path={'/'}>
          <Home />
        </RouteTransition>

        {hunters.map(hunter => (
          <RouteTransition key={hunter} exact={false} path={`/${hunter}`}>
            <Hunter hunter={hunter} />
          </RouteTransition>
        ))}
      </AnimatedRoutes>
    </>
  );
};

export default App;
