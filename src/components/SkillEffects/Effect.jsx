import React from 'react';
import PropTypes from 'prop-types';

import * as S from './Effect.styles';

const formatName = name => name.split(/(?=[A-Z])/).join(' ');

const Effect = props => {
  return (
    <S.Effect>
      {props.name !== 'none' ? <S.Name>{formatName(props.name)}</S.Name> : null}
      <S.Value>
        {typeof props.value !== 'function'
          ? props.value
          : props.skillRank >= 1
          ? props.value(props.skillRank)
          : props.value(1)}
      </S.Value>
    </S.Effect>
  );
};

Effect.propTypes = {
  name: PropTypes.string.isRequired,
  skillRank: PropTypes.number.isRequired,
  skillType: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
};

export default Effect;
