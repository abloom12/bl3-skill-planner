import React from 'react';
import PropTypes from 'prop-types';

import Effect from './Effect';

import * as S from './SkillEffects.styles';

const SkillEffects = props => {
  return (
    <>
      {/* Initial Rank */}
      {props.skillType === 'PASSIVE_ABILITY' ? (
        <S.Title>{props.skillRank >= 1 ? 'Current' : 'Next'} Rank:</S.Title>
      ) : null}

      {Object.entries(props.effects).map(([key, value]) => (
        <Effect
          key={key}
          name={key}
          value={value}
          skillType={props.skillType}
          skillRank={props.skillRank}
        />
      ))}

      {/* Next Rank */}
      {props.skillType === 'PASSIVE_ABILITY' &&
      props.skillRank >= 1 &&
      props.skillRank < props.skillMaxRank ? (
        <>
          <S.Title>Next Rank:</S.Title>
          {Object.entries(props.effects).map(([key, value]) => (
            <Effect
              key={key}
              name={key}
              value={value}
              skillType={props.skillType}
              skillRank={props.skillRank + 1}
            />
          ))}
        </>
      ) : null}
    </>
  );
};

SkillEffects.propTypes = {
  effects: PropTypes.object.isRequired,
  skillRank: PropTypes.number.isRequired,
  skillMaxRank: PropTypes.number.isRequired,
  skillType: PropTypes.string.isRequired,
};

export default SkillEffects;
