import styled from 'styled-components';

export const Effect = styled.div`
  display: flex;
`;

export const Name = styled.p`
  color: ${({ theme }) => theme.color.yellow};
  text-transform: capitalize;
  margin: 0 4px 0 0;
`;

export const Value = styled.p``;
