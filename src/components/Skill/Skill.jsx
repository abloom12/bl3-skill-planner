import React from 'react';
import PropTypes from 'prop-types';

import { types } from 'constants/index';

import * as S from './Skill.styles';

// Skill
const Skill = props => {
  const imgSrc = props.image ? require(`assets/images/${props.image}`) : null;
  const imgBgSrc = require(`assets/images/${props.imageBackground}`);

  return (
    <S.Skill>
      <S.Images purchased={props.purchased} treeColor={props.treeColor}>
        {imgSrc ? <S.Image src={imgSrc} alt={'skill'} /> : null}
        <S.ImageBg src={imgBgSrc} alt={'skillbg'} />
      </S.Images>

      {props.type === types.PASSIVE_ABILITY ? (
        <S.Points>
          <span>{`${props.rank ? props.rank : 0}/${props.maxRank}`}</span>
        </S.Points>
      ) : null}
    </S.Skill>
  );
};

Skill.propTypes = {
  equipped: PropTypes.bool,
  image: PropTypes.string.isRequired,
  imageBackground: PropTypes.string.isRequired,
  maxRank: PropTypes.number,
  purchased: PropTypes.bool.isRequired,
  rank: PropTypes.number,
  treeColor: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default Skill;
