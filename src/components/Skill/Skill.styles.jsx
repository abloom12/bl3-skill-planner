import styled, { css } from 'styled-components';

// Selected Skill Colors
const blue = css`
  filter: sepia(100%) hue-rotate(175deg) contrast(130%) saturate(300%);
`;
const green = css`
  filter: sepia(100%) hue-rotate(70deg) contrast(130%) saturate(300%);
`;
const red = css`
  filter: sepia(100%) hue-rotate(320deg) contrast(130%) saturate(300%);
`;
const purple = css`
  filter: sepia(100%) hue-rotate(250deg) contrast(130%) saturate(300%);
`;
const getActiveColor = props => {
  if (!props.purchased) return '';
  if (props.treeColor === 'green') return green;
  if (props.treeColor === 'blue') return blue;
  if (props.treeColor === 'red') return red;
  if (props.treeColor === 'purple') return purple;
};

export const Skill = styled.div`
  padding: 8px 5px;
  position: relative;
  min-width: 64px;
  max-width: 75px;

  @media (min-width: 375px) {
    min-width: 75px;
  }
`;
export const Image = styled.img`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 70%;
  z-index: 2;
`;
export const ImageBg = styled.img`
  position: relative;
  width: 100%;
  z-index: 1;
`;
export const Images = styled.div`
  position: relative;

  ${ImageBg} {
    ${({ purchased }) => (purchased ? getActiveColor : '')}
  }
`;

// Points
export const Points = styled.div`
  background: ${({ theme }) => theme.color.darkgray};
  color: ${({ theme }) => theme.color.white};
  font-size: 12px;
  line-height: 1;
  outline: 2px solid black;
  overflow: hidden;
  padding: 1px 4px 2px;
  position: absolute;
  bottom: 2px;
  right: 2px;
  z-index: 2;

  span {
    vertical-align: middle;
  }
`;
