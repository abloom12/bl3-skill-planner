import React from 'react';
import { useSelector } from 'react-redux';

import { MAX_SPENDABLE_POINTS } from 'constants/index';

import { getSpentPointsTotal } from 'redux/skills/selectors';

const PointBalance = props => {
  const spentPoints = useSelector(state =>
    getSpentPointsTotal(state, { hunter: props.hunter }),
  );

  return (
    <div style={{ marginBottom: '10px' }}>
      <h2>Skill Points: {MAX_SPENDABLE_POINTS - spentPoints}</h2>
    </div>
  );
};

export default PointBalance;
