import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { Route, Switch, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

const variants = {
  initial: { opacity: 0, x: '-100%' },
  animate: {
    opacity: 1,
    x: 0,
    zIndex: 1,
    transition: {
      type: 'spring',
      damping: 10,
      mass: 0.5,
      stiffness: 80,
      velocity: 2,
    },
  },
  exit: {
    opacity: 0,
    x: '100%',
    zIndex: 0,
    transition: {
      type: 'spring',
      damping: 10,
      mass: 0.5,
      stiffness: 150,
      velocity: 1,
    },
  },
};

export const RouteTransition = ({ children, exact = false, path, ...rest }) => {
  return (
    <Route exact={exact} path={path} {...rest}>
      <motion.div
        variants={variants}
        initial="initial"
        animate="animate"
        exit="exit">
        {children}
      </motion.div>
    </Route>
  );
};
RouteTransition.propTypes = {
  children: PropTypes.element,
  exact: PropTypes.bool,
  path: PropTypes.string,
};

export const AnimatedRoutes = ({
  children,
  exitBeforeEnter = true,
  initial = false,
}) => {
  const location = useLocation();

  return (
    <AnimatePresence exitBeforeEnter={exitBeforeEnter} initial={initial}>
      <Switch location={location} key={location.pathname}>
        {children}
      </Switch>
    </AnimatePresence>
  );
};
AnimatedRoutes.propTypes = {
  children: PropTypes.array,
  exitBeforeEnter: PropTypes.bool,
  initial: PropTypes.bool,
};
