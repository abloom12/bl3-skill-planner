import { useState } from 'react';

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false);

  const toggleIsShowing = () => setIsShowing(!isShowing);

  return [isShowing, toggleIsShowing];
};

export default useModal;
