import styled from 'styled-components';

export const Modal = styled.div`
  position: fixed;
  bottom: 0px;
  left: 0px;
  width: 100%;
`;

export const CloseBtn = styled.button`
  color: white;
`;
