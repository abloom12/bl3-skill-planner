import React from 'react';
import { createPortal } from 'react-dom';

import * as S from './Modal.styles';

export const Modal = ({ children }) => {
  return createPortal(<S.Modal>{children}</S.Modal>, document.body);
};

export default Modal;
