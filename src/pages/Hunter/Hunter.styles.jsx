import styled from 'styled-components';

export const HunterSkills = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`;

export const Points = styled.div`
  width: 100%;
`;

export const Trees = styled.div`
  max-width: 650px;
  width: 100%;
`;

export const Active = styled.div`
  width: 100%;
`;

export const Equipped = styled.div`
  flex-grow: 1;
  width: 30%;
`;
