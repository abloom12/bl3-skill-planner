import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { getSkillTrees } from 'redux/trees/selectors';

import PointBalance from 'components/PointBalance';
import SkillTree from 'components/SkillTree';
import ActiveSkill from 'components/ActiveSkill';
import EquippedSkills from 'components/EquippedSkills';
import Carousel from 'components/Carousel';
import * as S from './Hunter.styles';

const Hunter = props => {
  const trees = useSelector(state => getSkillTrees(state, props.hunter));

  return (
    <S.HunterSkills>
      <S.Points>
        <PointBalance hunter={props.hunter} />
      </S.Points>

      <S.Trees>
        <Carousel items={trees}>
          <Carousel.Nav />
          {trees.map(({ color, name, skillIds }, i) => (
            <Carousel.Item key={name} i={i}>
              <SkillTree
                hunter={props.hunter}
                treeColor={color}
                skillIds={skillIds}
              />
            </Carousel.Item>
          ))}
        </Carousel>
      </S.Trees>

      <S.Active>
        <ActiveSkill hunter={props.hunter} />
      </S.Active>

      <S.Equipped>
        <EquippedSkills hunter={props.hunter} />
      </S.Equipped>
    </S.HunterSkills>
  );
};

Hunter.propTypes = {
  hunter: PropTypes.string.isRequired,
};

export default Hunter;
