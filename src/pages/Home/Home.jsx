import React from 'react';

const Home = () => {
  return (
    <div>
      <h1>Welcome to the BL3 Skill Planner</h1>
      <p>Select a vault hunter from above to get started.</p>
    </div>
  );
};

export default Home;
