import * as amara from './amara';
import * as fl4k from './fl4k';
import * as moze from './moze';
import * as zane from './zane';

const SKILL_BACKGROUNDS = {
  ACTION_SKILL: 'ACTION_SKILL',
  PASSIVE_ABILITY: 'PASSIVE_ABILITY',
  ACTION_SKILL_AUGMENT: 'CHEVRON',
  ACTION_SKILL_EFFECT: 'CHEVRON',
  ACTION_SKILL_ELEMENT: 'DIAMOND',
  PET: 'DIAMOND',
};

const addData = (hunter, skills) => {
  return Object.entries(skills).reduce((acc, [id, skill]) => {
    acc[id] = {
      ...skill,
      hunter,
      id: id,
      image: `${hunter}/${id}.png`,
      imageBackground: `${SKILL_BACKGROUNDS[skill.type]}.png`,
    };

    if (skill.type === 'PASSIVE_ABILITY') {
      acc[id].rank = 0;
    } else {
      acc[id].equipped = false;
    }

    return acc;
  }, {});
};

export const skills = {
  amara: addData('amara', amara.skills),
  fl4k: addData('fl4k', fl4k.skills),
  moze: addData('moze', moze.skills),
  zane: addData('zane', zane.skills),
};

export const trees = {
  amara: Object.values(amara.trees),
  fl4k: Object.values(fl4k.trees),
  moze: Object.values(moze.trees),
  zane: Object.values(zane.trees),
};

// (function removeMouseOver() {
//   var skills = [...document.querySelectorAll('.relative.skill')];
//   skills.forEach(skill => {
//     var newSkill = skill.cloneNode(true);
//     skill.parentNode.replaceChild(newSkill, skill);
//   });
// })();
