import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import activeSkillReducer from './active/reducer';
import skillsReducer from './skills/reducer';
import treesReducer from './trees/reducer';

const rootReducer = combineReducers({
  activeSkill: activeSkillReducer,
  skills: skillsReducer,
  trees: treesReducer,
});

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') middlewares.push(logger);

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middlewares)),
);

export default store;
