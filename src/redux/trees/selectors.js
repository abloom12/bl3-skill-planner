import { createSelector } from 'reselect';

const getTrees = state => state.trees;

export const getSkillTrees = createSelector(
  [getTrees, (_, hunter) => hunter],
  (trees, hunter) => trees[hunter],
);
