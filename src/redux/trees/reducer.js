import { trees } from 'data/index';

const INITIAL_STATE = {
  amara: [...trees.amara],
  fl4k: [...trees.fl4k],
  moze: [...trees.moze],
  zane: [...trees.zane],
};

const treesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default: {
      return state;
    }
  }
};

export default treesReducer;
