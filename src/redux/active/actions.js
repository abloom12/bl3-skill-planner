import * as types from './types';

export const setActiveSkill = payload => ({
  type: types.SET_ACTIVE_SKILL,
  payload,
});

export const clearActiveSkill = () => ({
  type: types.CLEAR_ACTIVE_SKILL,
});
