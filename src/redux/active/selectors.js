import { createSelector } from 'reselect';

export const getActiveSkillId = createSelector(
  [state => state.activeSkill],
  active => active.id,
);
