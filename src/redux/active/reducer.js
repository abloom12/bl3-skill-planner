import produce from 'immer';

import * as types from './types';

const ACTIVE_STATE = { id: null };

const activeSkillReducer = (state = ACTIVE_STATE, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case types.SET_ACTIVE_SKILL: {
        return { id: action.payload };
      }
      case types.CLEAR_ACTIVE_SKILL: {
        return { id: null };
      }
    }
  });
};

export default activeSkillReducer;
