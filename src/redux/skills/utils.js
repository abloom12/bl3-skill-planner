import {
  getEquippedSkills,
  getEquippedElement,
  getEquippedEffect,
  getEquippedPet,
} from './selectors';

import * as types from 'constants/index';

const equipActionSkill = (state, { id, hunter }) => {
  const { equippedActionSkills, equippedAugments } = getEquippedSkills(
    state,
    hunter,
  );

  // Step Through Rules
  switch (hunter) {
    // AMARA & FL4K | max: 1
    case 'amara':
    case 'fl4k': {
      if (equippedActionSkills.length === 1) {
        // clear equipped augments
        equippedAugments.forEach(augment => {
          state[hunter][augment.id].equipped = false;
        });

        const equippedActionSkillId = equippedActionSkills[0].id;
        state[hunter][equippedActionSkillId].equipped = false;
        state[hunter][id].equipped = true;
      }

      state[hunter][id].equipped = true;
      return state;
    }
    // MOZE & ZANE | max: 2
    case 'moze':
    case 'zane': {
      if (equippedActionSkills.length === 2) {
        // clear equipped action skills
        equippedActionSkills.forEach(actionSkill => {
          state[hunter][actionSkill.id].equipped = false;
        });
        // clear equipped augments
        equippedAugments.forEach(augment => {
          state[hunter][augment.id].equipped = false;
        });
      }

      state[hunter][id].equipped = true;
      return state;
    }
  }
};
const equipAugment = (state, { id, hunter }) => {
  const { equippedActionSkills, equippedAugments } = getEquippedSkills(
    state,
    hunter,
  );

  // Step Through Rules
  switch (hunter) {
    // FL4K | max: 2 (2 per ACTION_SKILL)
    case 'fl4k': {
      if (
        equippedActionSkills.length === 1 &&
        equippedActionSkills[0].treeColor === state[hunter][id].treeColor
      ) {
        if (equippedAugments.length === 2) {
          equippedAugments.forEach(augment => {
            state[hunter][augment.id].equipped = false;
          });
        }

        // equip selected augment
        state[hunter][id].equipped = true;
      }

      return state;
    }
    // MOZE | max: 2 (1 per ACTION_SKILL)
    case 'moze': {
      if (equippedActionSkills.length > 0) {
        let augmentActionSkillEquipped;

        // get action skill in same tree as selected augment
        equippedActionSkills.forEach(actionSkill => {
          if (actionSkill.treeColor === state[hunter][id].treeColor) {
            augmentActionSkillEquipped = actionSkill;
          }
        });

        // if action skill in same tree is equipped
        if (augmentActionSkillEquipped) {
          if (equippedAugments.length >= 1) {
            // clear equipped augment by treeColor
            equippedAugments.forEach(augment => {
              if (augment.treeColor === augmentActionSkillEquipped.treeColor) {
                state[hunter][augment.id].equipped = false;
              }
            });
          }

          // equip selected augment
          state[hunter][id].equipped = true;
        }
      }

      return state;
    }
    // ZANE | max: 4 (2 per ACTION_SKILL)
    case 'zane': {
      if (equippedActionSkills.length > 0) {
        let augmentActionSkillEquipped;

        // get action skill in same tree as selected augment
        equippedActionSkills.forEach(actionSkill => {
          if (actionSkill.treeColor === state[hunter][id].treeColor) {
            augmentActionSkillEquipped = actionSkill;
          }
        });

        // if action skill in same tree is equipped
        if (augmentActionSkillEquipped) {
          // get equipped augments in same tree as selected augment
          let equippedAugmentsOfSelectedTree = equippedAugments.filter(
            augment =>
              augment.treeColor === augmentActionSkillEquipped.treeColor,
          );

          if (equippedAugmentsOfSelectedTree.length === 2) {
            equippedAugmentsOfSelectedTree.forEach(augment => {
              state[hunter][augment.id].equipped = false;
            });
          }

          // equip selected augment
          state[hunter][id].equipped = true;
        }
      }

      return state;
    }
  }
};
const equipElement = (state, { id, hunter }) => {
  const equippedElement = getEquippedElement(state);

  if (equippedElement.length === 1) {
    const equippedElementId = equippedElement[0].id;
    state[hunter][equippedElementId].equipped = false;
    state[hunter][id].equipped = true;
  }

  state[hunter][id].equipped = true;
};
const equipEffect = (state, { id, hunter }) => {
  const equippedEffect = getEquippedEffect(state);

  if (equippedEffect.length === 1) {
    const equippedEffectId = equippedEffect[0].id;
    state[hunter][equippedEffectId].equipped = false;
    state[hunter][id].equipped = true;
  }

  state[hunter][id].equipped = true;
};
const equipPet = (state, { id, hunter }) => {
  const equippedPet = getEquippedPet(state);

  if (equippedPet.length === 1) {
    const equippedPetId = equippedPet[0].id;
    state[hunter][equippedPetId].equipped = false;
    state[hunter][id].equipped = true;
  }

  state[hunter][id].equipped = true;
};

export const equipSkill = (state, { id, hunter }) => {
  switch (state[hunter][id].type) {
    case types.ACTION_SKILL: {
      return equipActionSkill(state, { id, hunter });
    }
    case types.ACTION_SKILL_AUGMENT: {
      return equipAugment(state, { id, hunter });
    }
    case types.ACTION_SKILL_ELEMENT: {
      return equipElement(state, { id, hunter });
    }
    case types.ACTION_SKILL_EFFECT: {
      return equipEffect(state, { id, hunter });
    }
    case types.PET: {
      return equipPet(state, { id, hunter });
    }
  }
};

export const addSkillPoint = (state, { id, hunter }) => {
  state[hunter][id].rank = state[hunter][id].rank + 1;
  return state;
};

export const removeSkillPoint = (state, { id, hunter }) => {
  state[hunter][id].rank = state[hunter][id].rank - 1;
  return state;
};
