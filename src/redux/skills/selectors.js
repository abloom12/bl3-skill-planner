import { createSelector } from 'reselect';

import * as types from 'constants/index';

// Helpers
//------------------
const createPropSelector = selector => (_, props) => selector(props);

// Prop Getters
//------------------
const getSkillIdProp = createPropSelector(props => props.id);
const getHunterProp = createPropSelector(props => props.hunter);
const getTreeColorProp = createPropSelector(props => props.treeColor);

// Local
//------------------
export const getEquippedSkills = createSelector(
  [
    (state, props) => {
      return state[props];
    },
  ],
  skills => {
    return Object.values(skills).reduce(
      (acc, val) => {
        if (!val.equipped) return acc;

        if (val.type === types.ACTION_SKILL) {
          acc.equippedActionSkills.push(val);
          return acc;
        }

        if (val.type === types.ACTION_SKILL_AUGMENT) {
          acc.equippedAugments.push(val);
          return acc;
        }

        return acc;
      },
      { equippedActionSkills: [], equippedAugments: [] },
    );
  },
);
export const getEquippedElement = createSelector(
  [state => state['amara']],
  skills => {
    return Object.values(skills).filter(
      skill => skill.type === types.ACTION_SKILL_ELEMENT && skill.equipped,
    );
  },
);
export const getEquippedEffect = createSelector(
  [state => state['amara']],
  skills => {
    return Object.values(skills).filter(
      skill => skill.type === types.ACTION_SKILL_EFFECT && skill.equipped,
    );
  },
);
export const getEquippedPet = createSelector(
  [state => state['fl4k']],
  skills => {
    return Object.values(skills).filter(
      skill => skill.type === types.PET && skill.equipped,
    );
  },
);

// Global
//------------------
const getSkills = state => state.skills;

export const getSkillData = createSelector(
  [getSkills, getSkillIdProp, getHunterProp],
  (skills, id, hunter) => {
    const data = { ...skills[hunter][id] };

    if (data.type === types.PASSIVE_ABILITY) {
      data.isPurchased = data.rank >= 1 ? true : false;
    } else {
      data.isPurchased = data.equipped;
    }

    return data;
  },
);
export const getAllEquippedSkills = createSelector(
  [getSkills, getHunterProp],
  (skills, hunter) =>
    Object.values(skills[hunter]).reduce(
      (acc, skill) => {
        const data = { ...skill };
        if (data.type === types.PASSIVE_ABILITY) {
          if (data.rank > 0) {
            data.isPurchased = data.rank >= 1 ? true : false;
            acc.passives.push(data);
          }
        } else {
          if (data.equipped) {
            data.isPurchased = data.equipped;
            acc.abilities.push(data);
          }
        }

        return acc;
      },
      {
        passives: [],
        abilities: [],
      },
    ),
);
export const getSpentTreePoints = createSelector(
  [getSkills, getHunterProp, getTreeColorProp],
  (skills, hunter, treeColor) =>
    Object.values(skills[hunter]).reduce((acc, skill) => {
      if (
        skill.type === types.PASSIVE_ABILITY &&
        skill.treeColor === treeColor
      ) {
        acc = acc + skill.rank;
      }

      return acc;
    }, 0),
);
export const getSpentPointsTotal = createSelector(
  [getSkills, getHunterProp],
  (skills, hunter) =>
    Object.values(skills[hunter]).reduce((acc, skill) => {
      if (skill.type === types.PASSIVE_ABILITY) {
        acc = acc + skill.rank;
      }

      return acc;
    }, 0),
);
