import * as types from './types';

export const addSkillPoint = ({ id, hunter }) => ({
  type: types.ADD_SKILL_POINT,
  payload: { id, hunter },
});

export const removeSkillPoint = ({ id, hunter }) => ({
  type: types.REMOVE_SKILL_POINT,
  payload: { id, hunter },
});

export const equipSkill = ({ id, hunter }) => ({
  type: types.EQUIP_SKILL,
  payload: { id, hunter },
});
