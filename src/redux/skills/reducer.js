import produce from 'immer';

import * as types from './types';
import * as util from './utils';

import { skills } from 'data/index';

const INITIAL_STATE = {
  amara: { ...skills.amara },
  fl4k: { ...skills.fl4k },
  moze: { ...skills.moze },
  zane: { ...skills.zane },
};

const skillsReducer = (state = INITIAL_STATE, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case types.ADD_SKILL_POINT: {
        return util.addSkillPoint(draft, action.payload);
      }
      case types.REMOVE_SKILL_POINT: {
        return util.removeSkillPoint(draft, action.payload);
      }
      case types.EQUIP_SKILL: {
        return util.equipSkill(draft, action.payload);
      }
    }
  });
};

export default skillsReducer;
