import React, { createContext, useMemo } from 'react';

export const ActiveHunterContext = createContext();

const ActiveHunterContextProvider = ({ hunter, children }) => {
  const value = useMemo(() => ({ hunter }), [hunter]);

  return (
    <ActiveHunterContext.Provider value={value}>
      {children}
    </ActiveHunterContext.Provider>
  );
};

export default ActiveHunterContextProvider;
